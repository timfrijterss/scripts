# Scripts

Collection of scripts, for use in different kinds of git projects.

These scripts are geared foremost for use in a CI. Yet, some scripts are added for use as a developer on the local
machine.

Some scripts are targeted at special environments, e.g., a specific CI, or a specific language. They are kept together
in one collection nevertheless. The overhead of having scripts in a project that are not applicable is negligible
relative to the overhead of splitting out this collection.

## Usage

Include this repository as a _submodule_ in your git repository at `scripts/common`, pointing to a tagged version of
this repository. You should add local scripts next to this submodule, in `scripts/`.

The inclusion should be done with an anonymous HTTP git URL as origin.

## Contents

- [npm](npm/README.md)
  - [`authenticateWithRegistry.sh`](npm/authenticateWithRegistry.md): create an `~/.npmrc file` with credentials to
    access the npm registry defined in the project-specific `.npmrc`
  - [`testOnNode10-8-6.sh`](npm/testOnNode10-8-6.md): for all major Node LTS versions, do a clean `npm install` and
    `npm test` in that version, and end with a clean `npm install` in the most recent major LTS version
  - [`testCi.sh`](npm/testCi.md): test an `npm` project on a Continuous Integration platform
- [git](git/README.md)
  - [`tagTravisGithub.sh`](git/tagTravisGithub.md): tag a successful Travis run on Github
