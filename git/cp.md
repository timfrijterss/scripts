# Copy a git repository from one remote to another

## Requires

`git` in your path.

## Usage

    > ./scripts/git/cp <target_git_uri> <target_git_uri>

E.g., to copy the repository `ppwcode/scripts` at Bitbucket to a repository with the same name at Github, use `ssh` at
both ends:

    > ./scripts/git/cp git@ppwcodeBitbucket:ppwcode/scripts.git git@ppwcodeGithub:ppwcode/scripts.git

The target repository may exist already. If there are no conflicts, this is a 'sync' operation.

## What it does

The script creates a bare mirror copy in `/tmp`, mirrors that to the target, and then cleans up.
