#!/usr/bin/env bash

#    Copyright 2018-2018 PeopleWare n.v.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

if [[ $# -ne 2  ]]; then
    echo "ERROR: requires a source and target git URI as arguments"
    exit 1
fi

echo Creating temporary directory …
tmpdir=`mktemp -d` || exit 1

echo Temporary directory created. Mirror cloning $1 into $tmpdir …
git clone --mirror $1 ${tmpdir}
cd ${tmpdir}

echo Cloning $1 done. Adding remote $2 to $tmpdir …
git remote add target $2

echo Adding remote $2 done. Fetching from both remotes …
git fetch origin --tags

echo Fetching done. Pushing to remote $2 …
git push target --mirror

echo Push to $2 done. Deleting $tmpdir …
rm -Rf ${tmpdir}

echo Done.
