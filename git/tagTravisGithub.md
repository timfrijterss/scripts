# Tag a successful Travis run on Github

## Prerequisites

This script requires

- to be run on [Travis] (or at least have environment variables `TRAVIS_REPO_SLUG` and `TRAVIS_BUILD_NUMBER`)
- a Github access token that grants write access to the git repository at Github, as a secret, in the environment
  variable `GITHUB_BACKPUSH_KEY`
- `git` in the `PATH`

## What it does

The scripts verifies that the environment variable `GITHUB_BACKPUSH_KEY` is set, and fails otherwise.

If the environment variable does exist, it tags the repository with `travis/${TRAVIS_BUILD_NUMBER}` (as
`Travis CI <travis@toryt.org>`, and pushes the tag to the Github repository.

The `TRAVIS_BUILD_NUMBER` is padded to 5 digits.

## Usage

On [Travis], it is important to _exempt_ the tags from building, to prevent getting in an infinite loop.

Use a separate `tag` stage to tag the repo:

`.travis.yml`:

    branches:
      except:
        - /^travis\/.*/
    language: …
    cache:
      directories:
        - …
    "os":
      - …
    jobs:
      include:
        # stage test
        - …
        …
      # stage tag
        - stage: tag
          install: skip
          script:
            …
            - scripts/common/git/tagTravisGithub.sh
            …

[travis]: https://travis-ci.org/
