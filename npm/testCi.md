# Test on CI

Test an `npm` project on a Continuous Integration platform.

We assume the project is checked-out by the Continuous Integration platform.

## What happens

- The environment variable `NODE_ENV="ci"` is set

  This is an industry standard, that allows automated tests to distinguish between locally run tests and tests run on a
  Continuous Integration platform.
- The time, the versions of `node` and `npm`, the checked out `git` sha, branch, and tags are echoed

  That way the situation of the test is retained in the logs with the test output.
- `npm audit` checks whether there are any known security issues with dependencies. 
- The `npm` project is installed, using [`npm ci`] This makes sure the versions mentioned in `package-lock.json` of all
  dependencies are used. This also executes `pre-` and `postinstall` scripts defined in `package.json`.
- `npm outdated` checks whether packages are up-to-date (this can only be done after install)
- Licenses are reported with `legally` and `tldrlegal`, using `npx`

  There are no criteria for a test to fail because of license issues, but developers should verify this output.
- Tests are run with `npm test`

  This also executes `pre-` and `posttest` scripts defined in `package.json`.

### `npm ci`

For `npm ci` (or `npm install`) to work, we need to be authenticated with the registry used by the project. See
[`authenticateWithRegistry.sh`](authenticateWithRegistry.md).

For `npm ci` to work, the project must have a `package-lock.json` (or `npm-shrinkwrap.json`).

`npm ci` is supported since `npm 5.7.0`, which is bundled with recent releases of `node 8` and later, but not, e.g.,
with `node 6`. For `node 6`, it is possible to upgrade `npm` to a more recent version. [`testNpm3.sh`](testNpm3.md) is
intended for use with Node 6, for as long as it remains in LTS (april 2019).

### License reports

The license reports are generated using `npx`. This means the `npm` packages this depends on do not have to be
dependencies of the project.

`npx` can be used since `npm 5.3.0`, which is bundled with recent releases of `node 8` and later, but not, e.g., with
`node 6`. For `node 6`, it is possible to upgrade `npm` to a more recent version.

### Caching

Since `npm ci` _removes any existing `node_modules` folder_, it does not make sense to cache that folder in CI.

Cache `~/.npm` instead.

## Usage on Bitbucket Pipelines

## Usage on Travis

[`npm ci`]: https://docs.npmjs.com/cli/ci.html
