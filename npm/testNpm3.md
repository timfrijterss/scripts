# Test on CI with npm 3

Test an `npm` project on a Continuous Integration platform with `npm v3`, which is the default on Node 6.

This is a script that can be used to test projects with Node 6, for as long as it is still LTS (until april 2019). The
full script, which will be carried into the future, is [`testCi.sh`].

We assume the project is checked-out by the Continuous Integration platform.

## What happens

- The environment variable `NODE_ENV="ci"`

  This is an industry standard, that allows automated tests to distinguish between locally run tests and tests run on a
  Continuous Integration platform.

- The time, the versions of `node` and `npm`, the checked out `git` sha, branch, and tags are echoed

  That way the situation of the test is retained in the logs with the test output.

- The `npm` project is installed, using `npm install`. _Note that `npm v3` does not know about `package-lock.json`, and
  thus does not use it._ This also executes `pre-` and `postinstall` scripts defined in `package.json`.
- Tests are run with `npm test`

  This also executes `pre-` and `posttest` scripts defined in `package.json`.

[`testCi.sh`] uses `npm ci` instead of `npm install`, which enforces the use of `package-lock.json`. Additionally, it
does some stuff using `npx`, which `npm v3` does not support.

### `npm install`

For `npm install` to work, we need to be authenticated with the registry used by the project. See
[`authenticateWithRegistry.sh`](authenticateWithRegistry.md).

### Caching

On CI, it makes sense to cache the `node_modules` folder between runs.

## Usage on Bitbucket Pipelines

## Usage on Travis

[`testci.sh`]: testCi.md
