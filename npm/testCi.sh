#!/usr/bin/env bash

#    Copyright 2018-2018 PeopleWare n.v.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


# error handling: see https://intoli.com/blog/exit-on-errors-in-bash-scripts/
# exit when any command fails
set -e

# keep track of the last executed command
trap 'last_command=$current_command; current_command=$BASH_COMMAND' DEBUG
# echo an error message before exiting
trap 'echo "Last command \"${last_command}\" command exited with code $?."' EXIT

# make clear to everybody that we are in CI
export NODE_ENV="ci"

# output the versions we are using
date
echo 'node:' `node --version`
echo 'npm:' `npm --version`

# output the situation
echo 'branch:' `git rev-parse --abbrev-ref HEAD`
echo 'tags:' `git name-rev --tags --name-only $(git rev-parse HEAD)`
echo 'sha:' `git rev-parse --verify HEAD`
echo "NODE_ENV:" ${NODE_ENV}

# are there known security issues?
echo 'are there known security issues?'
npm audit

# install dependencies from package-lock.json and test; we need to be authenticated with the registry
npm ci

# validate
echo 'are dependencies up-to-date?'
npm outdated
echo 'report licenses of dependencies'
npx legally -l -r
npx tldrlegal

# test
npm test
