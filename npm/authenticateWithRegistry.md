# Authenticate With Registry

Create an `~/.npmrc` file with credentials to access the `npm` `registry` defined in the project specific `.npmrc`, from
a secrect in the environment variable `NPM_REGISTRY_TOKEN`. With this setup, `npm` commands will be able to authenticate
to the `registry`.

## Prerequisites

This script requires

- `node`
- `npm` (installed with `node`),
- an `.npmrc` file at the root of the project, and
- an access token for the projects `npm` registry in the environment variable `$NPM_REGISTRY_TOKEN`.

## The need for authentication

An `npm` project depends on artifacts available from a remote registry. Some `npm` projects publish an artifact to a
remote registry.

If the `npm` project depends only on publicly available artifacts, and does not publish an artifact itself, no further
configuration is required.

However, when an `npm` project artifact is published to a registry, we need to authenticate to that registry with an
account that has write permissions. Public `npm` projects will be published in the [public `npm` registry]. Private
`npm` projects will be published in a private `npm`registry, such as [MyGet]. There is no difference in authentication.

Private `npm` projects might themselves depend on private artifacts, published in a private `npm`registry. Furthermore,
private `npm` projects often prefer to get their dependencies from a private `npm`registry, even if the artifact in
question is public, and available on the [public `npm` registry]. A private `npm`registry, such as [MyGet], can be set
up as _proxy_ for other repositories, and often offers central control over which artifacts and which versions are
available to the dependent projects. Some artifacts or versions of artifacts might be blocked. All artifacts used are
retained. The latter offers protection against a publicly available version of an artifact that your private projects
depend on becoming unavailable. [This has been known to happen]. In this case, we need to authenticate to the private
`npm` registry with an account that has read or read-write permissions, even before we can do `npm install`.

## .npmrc

An `npm` project has a need to authenticate with a registry, and uses this script, _must_ have an `.npmrc` file at its
root. See [npm config] for details.

The `.npmrc` file at the root of the project should at least have the following entries:

    scope = @<ORGANIZATION\*NAME>
    registry = <REGISTRY_URI>
    always-auth = true
    auth-type = legacy
    strict-ssl = true
    ca = null

Any `npm` project that follows our vernacular should be \_scoped\*, meaning that the name of the project in
`package.json` should be of the form

    {
      "name": "@<ORGANIZATION_NAME>/<ARTIFACT_NAME>",
      …
    }

The entry `scope = @<ORGANIZATION_NAME>` in the `.npmrc` file then makes sure that when `npm login` is done by a
developer, it is in the correct `scope` (`npm login` logs the user in into a _scope_).

The `registry = <REGISTRY_URI>` entry says that its value should be used as URI for all `npm` commands (such as
`npm install`, `npm cit`, `npm audit`, `npm publish`, …). If [MyGet] is used, the `<REGISTRY_URI>` will be of the form
`https://www.myget.org/F/<PRIVATE_FEED>/npm/`, where `<PRIVATE_FEED>` is the name of a feed under control of your
organisation.

## How to authenticate as developer

For the actions that require authentication to work, the developer needs to be 'logged in' in the `npm` registry / feed
setup in `.npmrc`, in the `scope` of the project. The developer should do

    npm login

and interactively provide her credentials. A token is then persisted in `~/.npmrc` (≠ the `.npmrc` file in the root of
the project).

Because of the settings in the `.npmrc` file above, this command actually is

    npm login --scope=@<ORGANIZATION_NAME> --registry <REGISTRY_URI> --always-auth --auth-type legacy --strict-ssl --ca null

When using [MyGet], the developer can use a _personal access token_ as app specific 'password'. _Personal access tokens_
can be created in the [MyGet] UI 'View Profile' / 'Access Tokens'. The access token should only have the minimal
required access rights.

## How to authenticate as automated process (e.g., CI)

`npm login` is an interactive command, and cannot be executed by an automated process. The script
[`authenticateWithRegistry.sh`](authenticateWithRegistry.sh) solves this. It creates a `~/.npmrc` for the running
process, in the format expected by `npm`, as if it was `npm login`.

The script is save, because it does not do anything if there already is a file at `~/.npmrc`. Otherwise, it creates an
`~/.npmrc` file in the expected format, based on value in the environment variable `$NPM_REGISTRY_TOKEN`. The entry is 1
line, that says that the value of `$NPM_REGISTRY_TOKEN` is to be used as token when communicating with the `registry`
defined in the project specific `.npmrc` file.

As a side effect, the `<REGISTRY_URI>` will also be available in the environment variable `$CI_NPM_REGISTRY` once the
script has run.

When using [MyGet], you can get an access token form the [MyGet] UI. The access token should only have the minimal
required access rights. It is ok to use a _personal access token_ from a particular team member, assuming it has minimal
rights. If that developer leaves the team, the value of the environment variable needs to be changed to a new access
token.

In Bitbucket, one _secret_ environment variable `$NPM_REGISTRY_TOKEN` can be set up at the team level for all projects
in the team.

It is good practice to rotate this token regularly.

## References

This script is based on

- [MyGet sample-pipelines-npm](https://bitbucket.org/myget/sample-pipelines-npm/src/master/bitbucket-pipelines.yml)
- [bcoe Private Modules Demo](https://bitbucket.org/benjamincoe/private-modules-demo/src/master/bitbucket-pipelines.yml)

[public `npm` registry]: https://www.npmjs.com/search?q=ppwcode
[myget]: https://www.myget.org
[this has been known to happen]: https://www.theregister.co.uk/2016/03/23/npm_left_pad_chaos/
[npm config]: https://docs.npmjs.com/misc/config
